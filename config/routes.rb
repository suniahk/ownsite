Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_pages#index'

  resources :contact_form, :path => 'contact', :only => [ :index, :create ], :as => :contacts
  resources :portfolio
  resources :sessions, only: [:create, :new] do
    collection do
      get :logout
    end
  end
end
