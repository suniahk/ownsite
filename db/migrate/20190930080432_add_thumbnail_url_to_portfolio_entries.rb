class AddThumbnailUrlToPortfolioEntries < ActiveRecord::Migration[5.1]
  def change
    add_column :portfolio_entries, :thumbnail_url, :string
  end
end
