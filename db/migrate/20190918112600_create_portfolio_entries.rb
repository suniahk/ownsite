class CreatePortfolioEntries < ActiveRecord::Migration[5.1]
  def change
    create_table :portfolio_entries do |t|
      t.string :name
      t.string :image
      t.text :description
      t.string :link

      t.timestamps
    end
  end
end
