class PortfolioAddDate < ActiveRecord::Migration[5.1]
  def change
    add_column :portfolio_entries, :date, :date
  end
end
