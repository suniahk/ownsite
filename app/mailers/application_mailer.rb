class ApplicationMailer < ActionMailer::Base
  default from: 'do.not.reply@alexwatson.ca'
  layout 'mailer'
end
