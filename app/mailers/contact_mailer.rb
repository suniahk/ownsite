class ContactMailer < ApplicationMailer
    def message_admin
        @contact = params[:contact]
        mail( to: "personal@alexwatson.ca", reply_to: @contact.email, subject: "You've received a new message on your site!" )
    end
end
