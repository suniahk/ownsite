class Contact
    include ActiveModel::Model, EmailValidatable

    attr_accessor :name, :email, :reason, :contents
    
    validates :name, :reason, :contents, presence: true
    validates :email, email: true, presence: true
end
