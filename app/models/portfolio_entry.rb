class PortfolioEntry < ApplicationRecord
    has_one_attached :screenshot

    validates :name, :screenshot, :date, :description, presence: true
end
