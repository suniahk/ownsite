require "mini_magick"

class PortfolioController < ApplicationController
    include ActionView::Helpers::NumberHelper

    def index
        @portfolioEntries = PortfolioEntry.order( date: :desc )
    end

    def new
        authentication_required!
        @portfolioEntry = PortfolioEntry.new( flash[:params] )
        
        @portfolioEntry.date = Date.today unless @portfolioEntry.date
    end

    def create
        authentication_required!
        @portfolioEntry = PortfolioEntry.new( portfolio_entry_params )

        if @portfolioEntry.valid? && @portfolioEntry.save
            flash[:alert] = "Project Added Successfully"
            redirect_to action: "index"
            return
        else
            flash[:error] = "Something went wrong.  Please try again."
        end

        flash[:params] = portfolio_entry_propagated_params
        redirect_to action: "new"
    end

    def destroy
        if PortfolioEntry.delete params[:id]
           flash[:alert] = "Entry deleted successfully."
        else
            flash[:error] = "Something went wrong.  Please try deleting again." 
        end
        redirect_to action: "index"
    end

    def portfolio_entry_params
        params.require(:portfolio_entry).permit(:name, :screenshot, :description, :date, :link)
    end

    def portfolio_entry_propagated_params
        params.require(:portfolio_entry).permit(:name, :description, :date, :link)
    end
end
