class SessionsController < ApplicationController
    def new
        @site_user = SiteUser.new( flash[:params] )
      end
    
      def create    
        @site_user = SiteUser.new( site_user_params )
    
        if @site_user.login_valid?
          session[:current_user] = true
          redirect_to '/'
        else
          @site_user.password = nil
          flash[:alert] = "Invalid Login."
          flash[:params] = site_user_params
          redirect_to action: "new"
        end
      end

      def logout
        reset_session
        redirect_to root_path
      end

      def site_user_params
        params.require(:site_user).permit(:username, :password)
    end
end
