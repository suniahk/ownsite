class ContactFormController < ApplicationController
    self.per_form_csrf_tokens = true

    def index
        @contact = Contact.new( flash[:params] )
    end

    def create
        @contact = Contact.new( contact_params )

        if verify_recaptcha(action: 'contact', minimum_score: 0.25) && @contact.valid?
            ContactMailer.with( contact: @contact ).message_admin.deliver_now
            flash[:alert] = "Thanks!  Your message has been sent.  Expect a response soon!"

        elsif !verify_recaptcha(action: 'contact', minimum_score: 0.25)
            flash[:params] = @contact
            flash[:error] = "Recaptcha failed.  If you're not a bot, try submitting the form again."
        else
            flash[:params] = @contact
            flash[:error] = "Something doesn't look right with your form.  Check the errors and try again."
        end
        
        redirect_to action: "index"
    end

    def contact_params
        params.require(:contact).permit(:name, :email, :reason, :contents)
    end
end
