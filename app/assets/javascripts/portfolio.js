// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
//= tobi.min.js

window.onload = () => {
    const tobi = new Tobi( {
        captionAttribute: "data-caption"
    } )
}