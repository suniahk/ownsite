module ApplicationHelper
    def nav_link( link_text, link_path, options={} )
        activeClass = current_page?( link_path ) ? 'active' : ''
        puts link_path

        if options[:class]
            options[:class] += ' ' + activeClass
        else
            options = options.merge(:class => activeClass)
        end

        link_to(raw(link_text), link_path, options).html_safe
    end
end
