describe ApplicationHelper do
    describe '#nav_link' do
        context 'generating links' do
           it 'generates the root link' do
                expect(self).to receive(:current_page?).and_return(true)
                expect( nav_link( 'root', '/' ) ).to include( "active" )
           end

           it 'generates the portfolio link' do
            expect(self).to receive(:current_page?).and_return(true)
            expect( nav_link( 'Portfolio', '/portfolio' ) ).to include( "active" )
       end
        end
    end
end